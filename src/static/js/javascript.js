$( document ).ready(function() {  
	$('#searchInput').on("input",function() {
		var search = $('#searchInput').val().toLowerCase();
		$('#tab-content li').each(function() {
			if($(this).attr('id').toLowerCase().indexOf(search)>-1)
			{
				$(this).show();
			}
			else{
				$(this).hide();
			}
		});
	});
	$(function () {
	$('[data-toggle="popover"]').popover()
		})
	$('#searchInput').keyup(function(e){
		if(e.keyCode == 13)
		{
			$("#search-form").attr("action", "/search/" + $('#searchInput').val());
			$("#search-form").submit();
		}
	});
	
	if(window.location.href.indexOf("/library/") == -1) {
       $('#uploadButton').hide();
    }
});
	