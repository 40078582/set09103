#imports
import os, time, ConfigParser, logging, flask, mutagen

#specific imports
from flask import Flask, render_template, url_for, redirect, request, session, abort
from mutagen.id3 import ID3, TIT2, TIT3, TALB, TPE1, TRCK, TYER
from logging.handlers import RotatingFileHandler

#define placeholders so refreshign page is easier, wipe after users logs out
artists=[]
albums=[]
songs=[]
genres=[]
years=[]
stuff=[]

# checks if their is an active session
def checkSession():
  try:
    if(session['name']):
      pass
  except KeyError:
    pass
    app.logger.info("invalid session")
    return redirect(url_for('sessionerror'))

#create app
app = Flask(__name__)

@app.route('/')
def homepage():
  return redirect(url_for('login'))

@app.route('/library/<username>', methods=['GET', 'POST'])
def library(username):
  error=''
  try:
    checkSession()
    username=session['name']
    if request.method == 'POST':
      file = request.files['datafile']
      app.logger.info(username+" attempting to upload file:"+file.filename)
      path = 'static/Users/'+username+'/'+str(file.filename)
      if os.path.isfile(path):
        app.logger.info(username+": "+file.filename+" already exists, canceling upload")
        error='File:'+ file.filename +' already exists'
      if not str(file.content_type.lower())=='audio/mp3':
        app.logger.info(username+": upload file rejected, grounds: not .mp3")
        error="You can only upload .mp3 files"
      if error=='':
        file.save(path)
        data(path)
  except Exception as e:
    error=str(e)
    return render_template('library.html', username=username, artists=artists, albums=albums, songs=songs, genres=genres, years=years, error=error)
  else:
    if artists:
      return render_template('library.html', username=session['name'], artists=artists, albums=albums, songs=songs, genres=genres, years=years, error=error)
    usernameDir = username+'/'
    dir=os.path.join("static/Users/",usernameDir)
    for filename in os.listdir(dir):
      path = dir+"/"+filename
      try:
        data(path)
      except Exception as e:
        error=e
        if '//password.txt: too small' in str(error):
          error=''
  return render_template('library.html', username=session['name'], artists=artists, albums=albums, songs=songs, genres=genres, years=years, error=error)

def data(path):
  error=''
  username=session['name']
  file='%s' % path
  artist=''
  album=''
  title=''
  genre=''
  year=''
  audio = ID3(path)
  try:
    artist = str(audio['TPE1'])
  except Exception as e:
    app.logger.info(username+" TPE1 read failure")
    error=error+str(e)
  try:
    album = str(audio['TALB'])
  except Exception as e:
    app.logger.info(username+" TALB read failure")
    error=error+str(e)
  try:
    title = str(audio['TIT2'])
  except Exception as e:
    app.logger.info(username+" TIT2 read failure")
    error=error+str(e)
  try:
    genre = str(audio['TCON'])
  except Exception as e:
    app.logger.info(username+" TCON read failure")
    error=error+str(e)
  try:
    year = str(audio['TDRC'])
  except Exception as e:
    app.logger.info(username+" TDRC read failure")
    error=error+str(e)
  dataArtist=''
  dataAlbum=''
  dataSong=''
  dataGenre=''
  dataYear=''
  if artist=='':
    artist='Unknown'
    artist.append(artist)
  elif artist not in artists:
    artists.append(artist)
  if album=='':
    album='Unknown'
  elif album not in albums:
    albums.append(album)
  if title=='':
    title='Unknown'
  songs.append(title)
  if genre=='':
    genre='Unknown'
    genres.append(genre)
  elif genre not in genres:
    genres.append(genre)
  if year=='':
    year='Unknown'
    years.append(year)
  elif year[:4] not in years:
    years.append(year[:4])
  dataArtist = artist
  dataAlbum=album
  dataSong = title
  dataGenre = genre
  dataYear = year[:4]
  stuff.append([dataArtist,dataAlbum,dataSong,dataGenre,dataYear,path])
  artists.sort()
  albums.sort()
  songs.sort()
  genres.sort()
  years.sort()
  return

@app.route('/signup', methods=['GET', 'POST'])
def signup():
  error = ''
  try:
    if request.method == "POST":
      attempted_username = request.form['username']
      attempted_password = request.form['password']
      confirm_password = request.form['confirmPassword']
      userFolder = 'static/Users/'+attempted_username
      if os.path.exists(userFolder):
        error='Username is already taken'
        app.logger.info(attempted_username+" already taken")
        return render_template("signup.html", error=error)
      if attempted_password!=confirm_password:
        error='Passwords do not match'
        app.logger.info("signup password failure")
        return render_template("signup.html", error=error)
      userDir=str("static/Users/"+attempted_username)
      os.makedirs(userDir)
      app.logger.info(attempted_username+' user folder created')
      with open(str('static/Users/'+attempted_username+'/password.txt'), 'w') as file:
        file.write(attempted_password)
        app.logger.info(attempted_username+' password file created')
      session['name']=attempted_username
      app.logger.info(attempted_username+' session started')
      return redirect(url_for('library', username=attempted_username))
    else:
      return render_template("signup.html", error=error)
  except Exception as e:
    return render_template("signup.html", error=e)

@app.route('/login', methods=['GET', 'POST'])
def login():
  error=''
  try:
    if session['name']:
      return redirect(url_for('library',username=session['name']))
  except KeyError:
    app.logger.info("invalid session")
    pass
  try:
    if request.method == "POST":
      attempted_username = request.form['username']
      attempted_password = request.form['password']
      userFolder = 'static/Users/'+attempted_username
      if not os.path.exists(userFolder):
        error='User does not exist'
        return render_template("login.html", error=error)
      with open(os.path.join(userFolder,"password.txt")) as f:
         passwordData=f.read()
      if not passwordData==attempted_password:
        error="Invalid Credentials"
        return render_template("login.html", error=error)
      session['name']=attempted_username
      app.logger.info(attempted_username+' session started')
      return redirect(url_for('library',username=attempted_username))
    else:
      return render_template("login.html", error=error)
  except Exception as e:
    return render_template("login.html", error=error)

@app.route("/artist/<artist>")
def artist(artist):
  checkSession()
  username=session['name']
  returnAlbums=[]
  returnSongs=[]
  search="%s" % artist
  for sub_list in stuff:
    if sub_list[0]==search:
      if sub_list[1] not in returnAlbums:
        returnAlbums.append(sub_list[1])
      returnSongs.append(sub_list[2])
  return render_template('artist.html', username=username, albums=returnAlbums, songs=returnSongs, title=search)

@app.route("/album/<album>")
def album(album):
  checkSession()
  search='%s' % album
  returnSongs=[]
  username=session['name']
  for sub_list in stuff:
    if sub_list[1]==search:
      returnSongs.append(sub_list[2])
  return render_template('album.html', username=username, songs=returnSongs, title=search)

@app.route("/genre/<genre>")
def genre(genre):
  checkSession()
  search='%s' % genre
  returnArtists=[]
  returnAlbums=[]
  returnSongs=[]
  username=session['name']
  for sub_list in stuff:
    if sub_list[3]==search:
      if sub_list[0] not in returnArtists:
        returnArtists.append(sub_list[0])
      if sub_list[1] not in returnAlbums:
        returnAlbums.append(sub_list[1])
      returnSongs.append(sub_list[2])
  returnArtists.sort()
  returnAlbums.sort()
  returnSongs.sort()
  return render_template('genre.html', username=username, artists=returnArtists, albums=returnAlbums, songs=returnSongs, title=search) 

@app.route("/search/<search>")
def search(search):
  checkSession()
  search='%s' % search
  returnArtists=[]
  returnAlbums=[]
  returnSongs=[]
  returnGenres=[]
  returnYears=[]
  username=session['name']
  for sub_list in stuff:
    if search.lower() in sub_list[0].lower():
      if sub_list[0] not in returnArtists:
        returnArtists.append(sub_list[0])
    if search.lower() in sub_list[1].lower():
      if sub_list[1] not in returnAlbums:
        returnAlbums.append(sub_list[1])
    if search.lower() in sub_list[2].lower():
      returnSongs.append(sub_list[2])
    if search.lower() in sub_list[3].lower():
      if sub_list[3] not in returnGenres:
        returnGenres.append(sub_list[3])
    if search.lower() in sub_list[4].lower():
      if sub_list[4] not in returnYears:
        if not sub_list[4]=='Unknown':
          returnYears.append(sub_list[4])
        else:
          returnYears.append('Unknown')
  returnArtists.sort()
  returnAlbums.sort()
  returnSongs.sort()
  returnGenres.sort()
  returnYears.sort()  
  return render_template('search.html', username=username, artists=returnArtists, albums=returnAlbums, songs=returnSongs, genres=returnGenres, years=returnYears, title=search) 

@app.route("/song/<song>")
def song(song):
  checkSession()
  search='%s' % song
  returnArtists=[]
  returnAlbums=[]
  returnGenres=[]
  returnYears=[]
  username=session['name']
  for sub_list in stuff:
    if search.lower() in sub_list[2].lower():
      if sub_list[0] not in returnArtists:
        returnArtists.append(sub_list[0])
    if search.lower() in sub_list[2].lower():
      if sub_list[1] not in returnAlbums:
        returnAlbums.append(sub_list[1])
    if search.lower() in sub_list[2].lower():
      if sub_list[3] not in returnGenres:
        returnGenres.append(sub_list[3])
    if search.lower() in sub_list[2].lower():
      if sub_list[4] not in returnYears:
        if not sub_list[4]=='Unknown':
          returnYears.append(sub_list[4])
        else:
          returnYears.append('Unknown')
  returnArtists.sort()
  returnAlbums.sort()
  returnGenres.sort()
  returnYears.sort()
  return render_template('song.html', username=username, artists=returnArtists, albums=returnAlbums, genres=returnGenres, years=returnYears, title=search) 

@app.route('/account/<username>')
def account(username):
  username=session['name']
  file = "static/Users/"+username
  creation = time.ctime(os.path.getctime(file))
  return render_template('account.html', username=username, joined=creation)

@app.route('/signout')
def signout():
  session.pop('name', None)
  del artists[:]
  del albums[:]
  del songs[:]
  del genres[:]
  del years[:]
  del stuff[:]
  return redirect(url_for('login'))

@app.route('/about')
def about():
  return render_template('about.html')

# error handlers
@app.route('/sessionerror')
def error():
  render_template("sessionerror.html", error=error)
  
@app.errorhandler(400)
def page_not_found(error):
  return render_template('error.html', error=error, errorType="400")
  
@app.errorhandler(404)
def page_not_found(error):
  return render_template('error.html', error=error, errorType="404")

@app.errorhandler(405)
def method_not_found(error):
  return render_template('error.html', error=error, errorType="405")

@app.errorhandler(500)
def error500(error):
  return render_template("error.html", error=error, errorType="500")

# Configuration
@app.route('/config/')
def config():
  str = []
  str.append('Debug:'+app.config['DEBUG'])
  str.append('port:'+app.config['port'])
  str.append('url:'+app.config['url'])
  str.append('ip_address:'+app.config['ip_address'])
  str.append('allowed_extensions'+app.config['allowed_extensions'])
  return '\t'.join(str)

def init(app):
  config = ConfigParser.ConfigParser()
  try:
    config_location = "etc/defaults.cfg"
    config.read(config_location)
    app.config['DEBUG'] = config.get("config", "debug")
    app.config['ip_address'] = config.get("config", "ip_address")
    app.config['port'] = config.get("config", "port")
    app.config['url'] = config.get("config", "url")
    app.config['log_file']=config.get("logging","name")
    app.config['log_location']=config.get("logging","location")
    app.config['log_level']=config.get("logging","level")
    app.secret_key=config.get("config","secret_key")
  except:
    print "Could not read config file: ", config_location

def logs(app):
  log_pathname = app.config['log_location']+app.config['log_file']
  file_handler = RotatingFileHandler(log_pathname, maxBytes=1024*1024*10, backupCount=1024)
  file_handler.setLevel(app.config['log_level'])
  formatter = logging.Formatter("%(levelname)s | %(asctime)s | %(module)s | %(funcName)s | %(message)s")
  file_handler.setFormatter(formatter)
  app.logger.setLevel(app.config['log_level'])
  app.logger.addHandler(file_handler)

if __name__ == "__main__":
    init(app)
    logs(app)
    app.run(
      host=app.config['ip_address'],port=int(app.config['port']))
